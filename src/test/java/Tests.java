import com.beust.ah.A;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.testng.Assert.*;

public class Tests {
    WebDriver driver;
    JavascriptExecutor js;
    Actions action;
    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver() ;
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        js = (JavascriptExecutor) driver;
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://demoqa.com/");
        driver.manage().window().maximize();
    }
    @Test(priority = 1)
    public void checkTitle() {
        String title;
        title = driver.getTitle();
        assertEquals(title, "DEMOQA", "Title is not as expected");
    }
    @Test(priority = 2)
    public void checkInformationAfterSubmit() {
        List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", cardElements);
        cardElements.click();
        List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-0"));
        WebElement textBox = listItems.get(0);
        textBox.click();
        WebElement fullNameInput = driver.findElement(By.cssSelector("#userName"));
        fullNameInput.sendKeys("Afat Najafli");
        WebElement emailInput = driver.findElement(By.cssSelector("#userEmail"));
        emailInput.sendKeys("afetnajafli@gmail.com");
        WebElement currentAddressInput = driver.findElement(By.cssSelector("textarea[id=\"currentAddress\"]"));
        currentAddressInput.sendKeys("Baku, Azerbaijan");
        WebElement permanentAddressInput = driver.findElement(By.cssSelector("textarea[id=\"permanentAddress\"]"));
        permanentAddressInput.sendKeys("Qazax, Azerbaijan");
        WebElement submitBtn = driver.findElement(By.cssSelector("#submit"));
        js.executeScript("arguments[0].scrollIntoView(); ", submitBtn);
        submitBtn.click();
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".mb-1")));
        WebElement nameOutput = driver.findElement(By.cssSelector("#name"));
        WebElement emailOutput = driver.findElement(By.cssSelector("#email"));
        WebElement currentAddressOutput = driver.findElement(By.cssSelector("p#currentAddress"));
        WebElement permanentAddressOutput = driver.findElement(By.cssSelector("p#permanentAddress"));
        assertEquals(nameOutput.getText(), "Name:Afat Najafli", "Name is not as expected");
        assertEquals(emailOutput.getText(), "Email:afetnajafli@gmail.com", "Email is not as expected");
        assertEquals(currentAddressOutput.getText(), "Current Address :Baku, Azerbaijan", "Current address is not as expected");
        assertEquals(permanentAddressOutput.getText(), "Permananet Address :Qazax, Azerbaijan", "Permanent address is not as expected");
    } @Test(priority = 3) public void checkNotesIsSelected() {
        List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", cardElements);
        cardElements.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-1"));
        WebElement checkBox = listItems.get(0);
        checkBox.click();
        WebElement expandBtn = driver.findElement(By.cssSelector("button[title=\"Expand all\"]"));
        expandBtn.click();
        WebElement notesIcon = driver.findElement(By.cssSelector("label[for=\"tree-node-notes\"]>.rct-checkbox"));
        notesIcon.click();
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement result = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".text-success")));
        assertTrue(result.isDisplayed());
    } @Test(priority = 4)
       public void checkRightClickMeButton() {
        List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", cardElements);
        cardElements.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-4"));
        WebElement buttons = listItems.get(0);
        buttons.click();
        WebElement rightClickBtn = driver.findElement(By.cssSelector("#rightClickBtn"));
        action = new Actions(driver);
        action.contextClick(rightClickBtn).build().perform();
        WebElement rightClickMessage = driver.findElement(By.cssSelector("#rightClickMessage"));
        assertTrue(rightClickMessage.isDisplayed());
    } @Test(priority = 5) public void checkFileUploaded() {
        List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", cardElements);
        cardElements.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-7"));
        WebElement uploadAndDownload = listItems.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", uploadAndDownload);
        uploadAndDownload.click();
        WebElement uploadFile = driver.findElement(By.cssSelector("#uploadFile"));
        String path = "C:\\Users\\Ali Najafli\\Desktop\\MyJavaprojects\\Task16\\src\\main\\resources\\test.docx";
        uploadFile.sendKeys(path);
        WebElement text = driver.findElement(By.cssSelector("#uploadedFilePath"));
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#uploadedFilePath")));
        assertTrue(text.isDisplayed());
    } @Test(priority = 6) public void checkVisibilityOfTheButton() {
        List<WebElement>  cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardElements = cards.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", cardElements);
        cardElements.click();
        List <WebElement>  listItems = driver.findElements(By.cssSelector("ul>#item-8"));
        WebElement dynamicProperties = listItems.get(0);
        js.executeScript("arguments[0].scrollIntoView(); ", dynamicProperties);
        dynamicProperties.click();
        WebElement visibleAfterBtn = driver.findElement(By.cssSelector("#visibleAfter"));
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(visibleAfterBtn));
        assertTrue(visibleAfterBtn.isDisplayed());
    } @Test(priority = 7) public void checkNewTabIsOpened() {
        List<WebElement>  cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView(); ", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-0"));
        WebElement browserWindows = listItems.get(2);
        browserWindows.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement newTabBtn = driver.findElement(By.cssSelector("#tabButton"));
        newTabBtn.click();
        String mainWindow = driver.getWindowHandle();
        Set windowsId = driver.getWindowHandles();
        Iterator iterator = windowsId.iterator();
        while(iterator.hasNext()) {
            String childWindow = (String) iterator.next();
            if(!mainWindow.equalsIgnoreCase(childWindow)) { driver.switchTo().window(childWindow);
                WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
                assertTrue(h1.isDisplayed(),"This is not a new tab");
                driver.close();
            } } driver.quit();
    } @Test(priority = 8) public void checkInformationWrittenInPromptBox() {
        List <WebElement>  cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView(); ", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts = listItems.get(1);
        alerts.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement promptBtn = driver.findElement(By.cssSelector("#promtButton"));
        promptBtn.click();
        driver.switchTo().alert().sendKeys("Heyyyy");
        driver.switchTo().alert().accept();
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement promptResult = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#promptResult")));
        assertEquals(promptResult.getText(), "You entered Heyyyy", "The information does not match");
        // You can also check the version I've commented //Assert.assertTrue(promptResult.getText().equals("You entered Heyyyy"), "The information does not match");
    } @Test(priority = 9) public void checkAlertCanceled() {
        List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView(); ", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement alerts = listItems.get(1);
        alerts.click();
        WebElement confirmBtn = driver.findElement(By.cssSelector("#confirmButton"));
        confirmBtn.click();
        driver.switchTo().alert().dismiss();
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement confirmResult = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#confirmResult")));
        assertTrue(confirmResult.getText().equals("You selected Cancel"));
        // You can also check the version I've commented //Assert.assertEquals(confirmResult.getText(), "You selected Cancel", "The information does not match");
    } @Test(priority = 10)
    public void checkTextInTheFrame() {
        List <WebElement>  cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardAlertsFrameWindows = cards.get(2);
        js.executeScript("arguments[0].scrollIntoView(); ", cardAlertsFrameWindows);
        cardAlertsFrameWindows.click();
        List<WebElement>  listItems = driver.findElements(By.cssSelector(".menu-list>#item-2"));
        WebElement frames = listItems.get(1);
        js.executeScript("arguments[0].scrollIntoView(); ", frames);
        frames.click();
        WebElement iframe = driver.findElement(By.cssSelector("iframe#frame1"));
        driver.switchTo().frame(iframe);
        WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
        assertTrue(h1.isDisplayed());
    } @Test(priority = 11) public void checkFiveIsChose() { List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = cards.get(4);
        js.executeScript("arguments[0].scrollIntoView(); ", cardInteractions);
        cardInteractions.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
        WebElement selectable = listItems.get(3);
        js.executeScript("arguments[0].scrollIntoView(); ", selectable);
        selectable.click();
        WebElement grid = driver.findElement(By.cssSelector("a[id=\"demo-tab-grid\"]"));
        grid.click();
        List<WebElement>  gridListItems = driver.findElements(By.cssSelector("#row2>li"));
        WebElement five = gridListItems.get(1);
        five.click();
        WebDriverWait wait =  new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.attributeToBe(five, "class", "list-group-item active list-group-item-action"));
        assertEquals(five.getAttribute("class"), "list-group-item active list-group-item-action", "Five is not chosen");
    }
    @Test(priority = 12)
    public void checkDraggedAndDropped() {
        List <WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
        WebElement cardInteractions = cards.get(4);
        js.executeScript("arguments[0].scrollIntoView(); ", cardInteractions);
        cardInteractions.click();
        List <WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-3"));
        WebElement droppable = listItems.get(3);
        js.executeScript("arguments[0].scrollIntoView(); ", droppable);
        droppable.click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        WebElement dragMe = driver.findElement(By.cssSelector("#draggable"));
        WebElement dropHere = driver.findElement(By.cssSelector("#droppable"));
        action = new Actions(driver);
        action.dragAndDrop(dragMe, dropHere).build().perform();
        List <WebElement> texts = driver.findElements(By.cssSelector("div[id=\"droppable\"]>p"));
        WebElement textDropped = texts.get(0);
        Assert.assertEquals(textDropped.getText(), "Dropped!", "Drag me is not dropped!");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        // You can also check the version I've commented //Assert.assertTrue(textDropped.getText().equals("Dropped"));
    }
    @AfterMethod public void tearDown() {
        driver.quit(); } }